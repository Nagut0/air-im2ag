import $ from 'jquery';
import cookies from "core-js/internals/task";
$(() => {

});

function clearThresh(elem){
    $(elem).removeClass("high_thresh");
    $(elem).removeClass('medium_thresh');
    $(elem).removeClass('low_thresh');
}

function colorCo2(){
    for (const element of $('.co2_thresh')){
        clearThresh(element);
        if (parseInt(element.textContent) >= $('#highCo2').val())
        {
            element.classList.add('high_thresh');
        } else if (parseInt(element.textContent) >= $('#medCo2').val()) {
            element.classList.add('medium_thresh');
        } else {
            element.classList.add('low_thresh');
        }
    }
}

function colorTemp(){
    for (const element of $('.temp_thresh')){
        clearThresh(element);
        if (parseInt(element.textContent) >= $('#highTemp').val())
        {
            element.classList.add('high_thresh');
        } else if (parseInt(element.textContent) >= $('#medTemp').val()) {
            element.classList.add('medium_thresh');
        } else {
            element.classList.add('low_thresh');
        }
    }
}

function colorHum(){
    for (const element of $('.hum_thresh')){
        clearThresh(element);
        if (parseInt(element.textContent) >= $('#highHum').val())
        {
            element.classList.add('high_thresh');
        } else if (parseInt(element.textContent) >= $('#medHum').val()) {
            element.classList.add('medium_thresh');
        } else {
            element.classList.add('low_thresh');
        }
    }
}

function init(medCo2, highCo2, medTemp,
                       highTemp, medHum, highHum) {
    // ===========================
    // Mise à jour des Input
    $('#medCo2').val(medCo2);
    $('#highCo2').val(highCo2);
    $('#medTemp').val(medTemp);
    $('#highTemp').val(highTemp);
    $('#medHum').val(medHum);
    $('#highHum').val(highHum);
    // ===========================
    // Coloriage des cellules
    colorCo2();
    colorTemp();
    colorHum();
}

$(document).ready(function() {
    let table = $('#salles_table').DataTable();
    init(500, 800, 28, 38, 50, 75);

    function actu(){
        const start = document.getElementById('date_debut').value;
        const end = document.getElementById('date_fin').value;

        location.assign('https://localhost:8000/salles/'+start+'/'+end);
    }
    window.actu = actu;
});

window.colorCo2 = colorCo2;
window.colorTemp = colorTemp;
window.colorHum = colorHum;