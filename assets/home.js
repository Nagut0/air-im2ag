import $ from "jquery";

$(document).ready(function() {
    function actu(){
        const start = document.getElementById('date_debut').value;
        const end = document.getElementById('date_fin').value;
        const co2 = document.getElementById('inputCo2').value;
        const hum = document.getElementById('inputHum').value;
        const temp = document.getElementById('inputTemp').value;

        location.assign('https://localhost:8000/home/'+start+'/'+end+'/'+co2+'/'+hum+'/'+temp);
    }
    window.actu = actu;
});