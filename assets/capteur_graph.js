/* globals Chart:false */

import $ from "jquery";
// import loadingPath from './brand/Loading_2.gif';

function loadData(graph){
  let data = {
    labels: [
    ],
    datasets: [{
      data: [
      ],
      lineTension: 0,
      backgroundColor: 'transparent',
      borderColor: '#007bff',
      borderWidth: 1,
      pointBackgroundColor: '#007bff',
      pointRadius: 1
    }]
  }
  for (const element of graph){
    data.labels.push(new Date(element.date).toLocaleString('fr-FR', {timeZone: 'UTC'}));
    data.datasets[0].data.push(element.valeur);
  }
  return data;
}

function initializeGraph(canvasId, data, title){
  const ctxt = document.getElementById(canvasId)
  // eslint-disable-next-line no-unused-vars
  const chart = new Chart(ctxt, {
    type: 'line',
    data: data,
    options: {
      plugins: {
        legend: {
          display: false
        },
        tooltip: {
          boxPadding: 3
        },
        title: {
          display: true,
          text: title
        }
      }
    }
  })
}

function loadGraphs(co2Data, humData, tempData){
  'use strict'

  initializeGraph('co2Chart', loadData(co2Data), 'Co2 (ppm)');
  initializeGraph('humChart', loadData(humData), 'Humidité (%)');
  initializeGraph('tempChart', loadData(tempData), 'Température (°C)');
}

function eraseData(chart){
  chart.data.labels = [];
  chart.data.datasets.forEach((dataset) => {
    dataset.data = [];
  });
  chart.update();
}

function initData(chart, labels, data) {
  chart.data.labels = labels;
  chart.data.datasets[0].data = data;
  chart.update();
}

(() => {
  let httpRequest;

  let start_measure;

  function makeRequest() {
    start_measure = Date.now();
    document.getElementById('loader').style.display = 'block';

    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
      alert("Giving up :(Cannot create an XMLHTTP instance");
      return false;
    }
    httpRequest.onreadystatechange = updateData;
    const start = document.getElementById('date_debut').value;
    const end = document.getElementById('date_fin').value;

    // httpRequest.open("GET","https://localhost:8000/api/capteurs/366/"+start+"/"+end);
    httpRequest.open("GET","https://localhost:8000/api/capteurs/366/"+start+"/"+end);
    httpRequest.send();
  }

  function updateData(){
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {

        const data = JSON.parse(httpRequest.responseText);

        console.log(data);

        let cChart = Chart.getChart('co2Chart');
        let hChart = Chart.getChart('humChart');
        let tChart = Chart.getChart('tempChart');

        eraseData(cChart);
        eraseData(hChart);
        eraseData(tChart);

        if(data.length){
          initData(cChart, data[0]['l'], data[0]['v']);
          initData(hChart, data[1]['l'], data[1]['v']);
          initData(tChart, data[2]['l'], data[2]['v']);
        } else {
          alert("Aucune donnée trouvée.")
          initData(cChart, null, null);
          initData(hChart, null, null);
          initData(tChart, null, null);
        }
        const end_measure = Date.now();
        alert(`Execution time: ${end_measure - start_measure} ms`);

      } else {
        alert("There was a problem with the request.");
      }
      document.getElementById('loader').style.display = 'none';
    }
  }

  window.makeRequest = makeRequest;

})()

$(document).ready(function() {

  const co2Data = $.parseJSON(document.getElementById('co2Chart').dataset.co2Data);
  const humData = $.parseJSON(document.getElementById('humChart').dataset.humData);
  const tempData = $.parseJSON(document.getElementById('tempChart').dataset.tempData);

  loadGraphs(co2Data, humData, tempData);

});

