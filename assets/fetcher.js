import $ from "jquery";

$(document).ready(function() {
    function actu(){
        const start = document.getElementById('date_debut').value;
        const end = document.getElementById('date_fin').value;

        location.assign('https://localhost:8000/fetchDB/'+start+'/'+end);
    }
    window.actu = actu;
});