<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230605080543 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A169592419D3E');
        $this->addSql('DROP INDEX IDX_5B4A169592419D3E ON capteur');
        $this->addSql('ALTER TABLE capteur CHANGE salle_id_id salle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A1695DC304035 FOREIGN KEY (salle_id) REFERENCES salle (id)');
        $this->addSql('CREATE INDEX IDX_5B4A1695DC304035 ON capteur (salle_id)');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C729DB0FC');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C92419D3E');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C9A384383');
        $this->addSql('DROP INDEX IDX_FDCA8C9C92419D3E ON main');
        $this->addSql('DROP INDEX IDX_FDCA8C9C9A384383 ON main');
        $this->addSql('DROP INDEX IDX_FDCA8C9C729DB0FC ON main');
        $this->addSql('ALTER TABLE main ADD salle_id INT DEFAULT NULL, ADD enseignant_id INT DEFAULT NULL, ADD groupe_etu_id INT DEFAULT NULL, DROP salle_id_id, DROP enseignat_id_id, DROP groupe_etu_id_id');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9CDC304035 FOREIGN KEY (salle_id) REFERENCES salle (id)');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9CE455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignant (id)');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C4B76C9F3 FOREIGN KEY (groupe_etu_id) REFERENCES groupe_etudiant (id)');
        $this->addSql('CREATE INDEX IDX_FDCA8C9CDC304035 ON main (salle_id)');
        $this->addSql('CREATE INDEX IDX_FDCA8C9CE455FCC0 ON main (enseignant_id)');
        $this->addSql('CREATE INDEX IDX_FDCA8C9C4B76C9F3 ON main (groupe_etu_id)');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF833F445153');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF83C337EE11');
        $this->addSql('DROP INDEX IDX_DDABFF83C337EE11 ON releve');
        $this->addSql('DROP INDEX IDX_DDABFF833F445153 ON releve');
        $this->addSql('ALTER TABLE releve ADD capteur_id INT NOT NULL, ADD type_releve_id INT NOT NULL, DROP capteur_id_id, DROP type_releve_id_id, DROP relation');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF831708A229 FOREIGN KEY (capteur_id) REFERENCES capteur (id)');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF83E3D4A636 FOREIGN KEY (type_releve_id) REFERENCES type_releve (id)');
        $this->addSql('CREATE INDEX IDX_DDABFF831708A229 ON releve (capteur_id)');
        $this->addSql('CREATE INDEX IDX_DDABFF83E3D4A636 ON releve (type_releve_id)');
        $this->addSql('ALTER TABLE salle DROP FOREIGN KEY FK_4E977E5CC8ADFD5C');
        $this->addSql('DROP INDEX IDX_4E977E5CC8ADFD5C ON salle');
        $this->addSql('ALTER TABLE salle CHANGE batiment_id_id batiment_id INT NOT NULL');
        $this->addSql('ALTER TABLE salle ADD CONSTRAINT FK_4E977E5CD6F6891B FOREIGN KEY (batiment_id) REFERENCES batiment (id)');
        $this->addSql('CREATE INDEX IDX_4E977E5CD6F6891B ON salle (batiment_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE salle DROP FOREIGN KEY FK_4E977E5CD6F6891B');
        $this->addSql('DROP INDEX IDX_4E977E5CD6F6891B ON salle');
        $this->addSql('ALTER TABLE salle CHANGE batiment_id batiment_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE salle ADD CONSTRAINT FK_4E977E5CC8ADFD5C FOREIGN KEY (batiment_id_id) REFERENCES batiment (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_4E977E5CC8ADFD5C ON salle (batiment_id_id)');
        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A1695DC304035');
        $this->addSql('DROP INDEX IDX_5B4A1695DC304035 ON capteur');
        $this->addSql('ALTER TABLE capteur CHANGE salle_id salle_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A169592419D3E FOREIGN KEY (salle_id_id) REFERENCES salle (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_5B4A169592419D3E ON capteur (salle_id_id)');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF831708A229');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF83E3D4A636');
        $this->addSql('DROP INDEX IDX_DDABFF831708A229 ON releve');
        $this->addSql('DROP INDEX IDX_DDABFF83E3D4A636 ON releve');
        $this->addSql('ALTER TABLE releve ADD capteur_id_id INT NOT NULL, ADD type_releve_id_id INT NOT NULL, ADD relation VARCHAR(255) NOT NULL, DROP capteur_id, DROP type_releve_id');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF833F445153 FOREIGN KEY (type_releve_id_id) REFERENCES type_releve (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF83C337EE11 FOREIGN KEY (capteur_id_id) REFERENCES capteur (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_DDABFF83C337EE11 ON releve (capteur_id_id)');
        $this->addSql('CREATE INDEX IDX_DDABFF833F445153 ON releve (type_releve_id_id)');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9CDC304035');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9CE455FCC0');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C4B76C9F3');
        $this->addSql('DROP INDEX IDX_FDCA8C9CDC304035 ON main');
        $this->addSql('DROP INDEX IDX_FDCA8C9CE455FCC0 ON main');
        $this->addSql('DROP INDEX IDX_FDCA8C9C4B76C9F3 ON main');
        $this->addSql('ALTER TABLE main ADD salle_id_id INT DEFAULT NULL, ADD enseignat_id_id INT DEFAULT NULL, ADD groupe_etu_id_id INT DEFAULT NULL, DROP salle_id, DROP enseignant_id, DROP groupe_etu_id');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C729DB0FC FOREIGN KEY (groupe_etu_id_id) REFERENCES groupe_etudiant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C92419D3E FOREIGN KEY (salle_id_id) REFERENCES salle (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C9A384383 FOREIGN KEY (enseignat_id_id) REFERENCES enseignant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_FDCA8C9C92419D3E ON main (salle_id_id)');
        $this->addSql('CREATE INDEX IDX_FDCA8C9C9A384383 ON main (enseignat_id_id)');
        $this->addSql('CREATE INDEX IDX_FDCA8C9C729DB0FC ON main (groupe_etu_id_id)');
    }
}
