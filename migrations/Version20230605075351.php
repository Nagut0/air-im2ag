<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230605075351 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE batiment (id INT AUTO_INCREMENT NOT NULL, nom_batiment VARCHAR(254) NOT NULL, affiliation VARCHAR(254) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE capteur (id INT AUTO_INCREMENT NOT NULL, salle_id_id INT DEFAULT NULL, dev_eui VARCHAR(254) NOT NULL, marque VARCHAR(254) NOT NULL, reseau VARCHAR(254) DEFAULT NULL, description VARCHAR(254) DEFAULT NULL, date_debut DATETIME DEFAULT NULL, date_fin DATETIME DEFAULT NULL, INDEX IDX_5B4A169592419D3E (salle_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE main (id INT AUTO_INCREMENT NOT NULL, salle_id_id INT DEFAULT NULL, enseignat_id_id INT DEFAULT NULL, groupe_etu_id_id INT DEFAULT NULL, intitule_cours VARCHAR(254) DEFAULT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, INDEX IDX_FDCA8C9C92419D3E (salle_id_id), INDEX IDX_FDCA8C9C9A384383 (enseignat_id_id), INDEX IDX_FDCA8C9C729DB0FC (groupe_etu_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enseignant (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(254) NOT NULL, prenom VARCHAR(254) NOT NULL, email_enseignant VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe_etudiant (id INT AUTO_INCREMENT NOT NULL, code_vet VARCHAR(254) DEFAULT NULL, nom VARCHAR(254) NOT NULL, nombre_etudiants INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE releve (id INT AUTO_INCREMENT NOT NULL, capteur_id_id INT NOT NULL, type_releve_id_id INT NOT NULL, valeur DOUBLE PRECISION NOT NULL, date DATETIME NOT NULL, relation VARCHAR(255) NOT NULL, INDEX IDX_DDABFF83C337EE11 (capteur_id_id), INDEX IDX_DDABFF833F445153 (type_releve_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE salle (id INT AUTO_INCREMENT NOT NULL, batiment_id_id INT NOT NULL, nom_salle VARCHAR(254) NOT NULL, capacite INT DEFAULT NULL, INDEX IDX_4E977E5CC8ADFD5C (batiment_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_releve (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A169592419D3E FOREIGN KEY (salle_id_id) REFERENCES salle (id)');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C92419D3E FOREIGN KEY (salle_id_id) REFERENCES salle (id)');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C9A384383 FOREIGN KEY (enseignat_id_id) REFERENCES enseignant (id)');
        $this->addSql('ALTER TABLE main ADD CONSTRAINT FK_FDCA8C9C729DB0FC FOREIGN KEY (groupe_etu_id_id) REFERENCES groupe_etudiant (id)');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF83C337EE11 FOREIGN KEY (capteur_id_id) REFERENCES capteur (id)');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF833F445153 FOREIGN KEY (type_releve_id_id) REFERENCES type_releve (id)');
        $this->addSql('ALTER TABLE salle ADD CONSTRAINT FK_4E977E5CC8ADFD5C FOREIGN KEY (batiment_id_id) REFERENCES batiment (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A169592419D3E');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C92419D3E');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C9A384383');
        $this->addSql('ALTER TABLE main DROP FOREIGN KEY FK_FDCA8C9C729DB0FC');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF83C337EE11');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF833F445153');
        $this->addSql('ALTER TABLE salle DROP FOREIGN KEY FK_4E977E5CC8ADFD5C');
        $this->addSql('DROP TABLE batiment');
        $this->addSql('DROP TABLE capteur');
        $this->addSql('DROP TABLE main');
        $this->addSql('DROP TABLE enseignant');
        $this->addSql('DROP TABLE groupe_etudiant');
        $this->addSql('DROP TABLE releve');
        $this->addSql('DROP TABLE salle');
        $this->addSql('DROP TABLE type_releve');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
