<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230717125339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A1695DC304035');
        $this->addSql('DROP INDEX IDX_5B4A1695DC304035 ON capteur');
        $this->addSql('ALTER TABLE capteur DROP salle_id');
        $this->addSql('ALTER TABLE releve ADD salle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE releve ADD CONSTRAINT FK_DDABFF83DC304035 FOREIGN KEY (salle_id) REFERENCES salle (id)');
        $this->addSql('CREATE INDEX IDX_DDABFF83DC304035 ON releve (salle_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE capteur ADD salle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A1695DC304035 FOREIGN KEY (salle_id) REFERENCES salle (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_5B4A1695DC304035 ON capteur (salle_id)');
        $this->addSql('ALTER TABLE releve DROP FOREIGN KEY FK_DDABFF83DC304035');
        $this->addSql('DROP INDEX IDX_DDABFF83DC304035 ON releve');
        $this->addSql('ALTER TABLE releve DROP salle_id');
    }
}
