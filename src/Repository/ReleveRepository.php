<?php

namespace App\Repository;

use App\Entity\Capteur;
use App\Entity\Releve;
use App\Entity\Salle;
use App\Entity\TypeReleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Releve>
 *
 * @method Releve|null find($id, $lockMode = null, $lockVersion = null)
 * @method Releve|null findOneBy(array $criteria, array $orderBy = null)
 * @method Releve[]    findAll()
 * @method Releve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReleveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Releve::class);
    }

    public function save(Releve $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Releve $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getSalleFromCapteur(Capteur $capteur): null|Salle
    {
        $qb = $this->createQueryBuilder('r');
        $qb->andWhere('r.capteur = :capteur')
            ->setParameter('capteur', $capteur);
        $qb->orderBy('r.date', 'DESC');
        $qb->setMaxResults(1);

        $res = $qb->getQuery()->getResult();

        if (empty($res)){
            return null;
        }

        return $res[0]->getSalle();
    }

    public function getCapteursFromSalle(Salle $salle): null|array{

        $capteurs = $this->getEntityManager()->getRepository(Capteur::class)->findAll();

        $data = [];

        foreach($capteurs as $c){
            $temp = $this->getSalleFromCapteur($c);
            if ($temp === $salle){
                $data[] = $c;
            }
        }

        return $data;
    }

    # Récupère des relevés selon des informations fournies
    public function getReleves(TypeReleve $type = null,
                               Capteur $capteur = null,
                               Salle $salle = null,
                               bool $latest = false,
                               int $nb_results = null,
                                \DateTime $start = null,
                                \DateTime $end = null,
                                bool $forGraph = false,): array
    {
        $qb = $this->createQueryBuilder('r');

        # Si les relevés sont à afficher dans un graph
        if($forGraph){
            $qb->select('r.date, r.valeur');
        }
        # Si un type de relevé est précisé
        if($type){
            $qb->andWhere('r.type_releve = :type')
                ->setParameter('type', $type);
        }
        # Si un capteur est précisé
        if($capteur){
            $qb->andWhere('r.capteur = :capteur')
                ->setParameter('capteur', $capteur);
        }
        #Si une salle est précisée
        if($salle){
            $qb->andWhere('r.salle = :salle')
                ->setParameter('salle', $salle);
        }
        #Si une date de départ est précisée
        if($start){
            $qb->andWhere('r.date >= :start')
                ->setParameter('start', $start);
        }
        #Si une date de fin est précisée
        if($end){
            $qb->andWhere('r.date <= :end')
                ->setParameter('end', $end);
        }
        # Si on doit trier par ordre croissant ou décroissant
        if($latest){
            $qb->orderBy('r.date', 'DESC');
        } else {
            $qb->orderBy('r.date', 'ASC');
        }
        # Si un nombre maximal de résultats est précisé
        if($nb_results){
            $qb->setMaxResults($nb_results);
        }

        return $qb->getQuery()->getResult();
    }

    public function getStatsSalle(Salle     $salle,
                                  \DateTime $start = null,
                                  \DateTime $end = null): array
    {
        $qb = $this->createQueryBuilder('r')
            ->select(" rt.label, min(r.valeur), avg(r.valeur), max(r.valeur)")
            ->andWhere('r.salle = :salle')
            ->setParameter('salle', $salle)
            ->groupBy('r.type_releve')
            ->join('r.type_releve', 'rt')
        ;

        if($start){
            $qb->andWhere('r.date >= :start')
                ->setParameter('start', $start);
        }
        if($end) {
            $qb->andWhere('r.date <= :end')
                ->setParameter('end', $end);
        }

        return $qb->getQuery()->getResult();
    }

    # Renvoie le nombre de relevé dépassant un seuil pour un type de relevé sur une plage de date
    public function getNbReleves(\DateTime $start = null,
                                    \DateTime $end = null,
                                    int|float $threshold,
                                    TypeReleve $type): array
    {

        $qb = $this->createQueryBuilder('r')
            ->select('(rs.nom_salle), count(r.id), (r.salle)')
            ->join('r.capteur', 'rc')
            ->join('r.type_releve', 'rt')
            ->join('r.salle', 'rs')
            ->groupBy('r.salle')
        ;

        if($start){
            $qb->andWhere('r.date >= :start')
                ->setParameter('start', $start);
        }
        if($end) {
            $qb->andWhere('r.date <= :end')
                ->setParameter('end', $end);
        }

        $qb->andWhere('r.type_releve = :type')
            ->setParameter('type', $type);
        $qb->andWhere('r.valeur >= :threshold')
            ->setParameter('threshold', $threshold);

        return $qb->getQuery()->getResult();
    }

//    /**
//     * @return Releve[] Returns an array of Releve objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Releve
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
