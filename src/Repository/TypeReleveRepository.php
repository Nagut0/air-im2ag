<?php

namespace App\Repository;

use App\Entity\TypeReleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypeReleve>
 *
 * @method TypeReleve|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeReleve|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeReleve[]    findAll()
 * @method TypeReleve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeReleveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeReleve::class);
    }

    public function save(TypeReleve $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TypeReleve $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    # Renvoie un TypeReleve aléatoire
    public function randRow(): TypeReleve|null {
        $all = $this->findAll();
        shuffle($all);
        return $all[0];
    }

//    /**
//     * @return TypeReleve[] Returns an array of TypeReleve objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TypeReleve
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
