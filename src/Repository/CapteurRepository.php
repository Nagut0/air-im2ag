<?php

namespace App\Repository;

use App\Entity\Capteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function PHPUnit\Framework\isEmpty;

/**
 * @extends ServiceEntityRepository<Capteur>
 *
 * @method Capteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Capteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Capteur[]    findAll()
 * @method Capteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CapteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Capteur::class);
    }

    public function save(Capteur $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Capteur $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    # Renvoie un Capteur aléatoire
    public function randRow(): Capteur|null {
        $all = $this->findAll();
        shuffle($all);
        return $all[0];
    }

    # Renvoie un capteur sur la base de son devEui
    public function findByDevEui($devEui): Capteur|null{
        $temp = $this->findBy(array('dev_eui' =>$devEui));
        if(sizeof($temp)>1){
            dump("Plus d'un capteur trouvé");
            dd($temp);
        }
        if(empty($temp)){
            dump("Aucun capteur trouvé");
            return null;
        }
        return $temp[0];
    }

//    /**
//     * @return Capteur[] Returns an array of Capteur objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Capteur
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
