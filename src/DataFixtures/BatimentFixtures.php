<?php

namespace App\DataFixtures;

use App\Entity\Batiment;
use App\Entity\Enseignant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BatimentFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i=0; $i < 20; $i++){
            $batiment = new Batiment();
            $batiment->setNomBatiment("Batiment$i");
            $batiment->setAffiliation("Affiliation".($i%5));

            $manager->persist($batiment);
        }

        $manager->flush();
    }
}
