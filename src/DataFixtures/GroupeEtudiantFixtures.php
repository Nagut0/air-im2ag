<?php

namespace App\DataFixtures;

use App\Entity\Batiment;
use App\Entity\GroupeEtudiant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class GroupeEtudiantFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $group = new GroupeEtudiant();
            $group->setNom("Groupe$i");
            $group->setNombreEtudiants($faker->randomDigit);
            $group->setCodeVet("Code$i");

            $manager->persist($group);
        }

        $manager->flush();
    }
}
