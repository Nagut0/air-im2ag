<?php

namespace App\DataFixtures;

use App\Entity\Batiment;
use App\Entity\Salle;
use App\Service\DoctrineRandom;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SalleFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $salle = new Salle();
            $salle->setNomSalle("Salle$i");
            $salle->setCapacite($faker->randomDigit);
            $salle->setBatiment($this->doctrine->getRepository(Batiment::class)->randRow());

            $manager->persist($salle);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            BatimentFixtures::class
        ];
    }
}
