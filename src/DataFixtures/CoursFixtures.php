<?php

namespace App\DataFixtures;

use App\Entity\Cours;
use App\Entity\Enseignant;
use App\Entity\GroupeEtudiant;
use App\Entity\Salle;
use App\Service\DoctrineRandom;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CoursFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {

    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $cours = new Cours();
            $cours->setIntituleCours("Cours$i");
            $cours->setDateDebut($faker->dateTimeBetween('-1 days','now'));
            $cours->setDateFin($faker->dateTime);
            $cours->setSalle($this->doctrine->getRepository(Salle::class)->randRow());
            $cours->setEnseignant($this->doctrine->getRepository(Enseignant::class)->randRow());
            $cours->setGroupeEtu($this->doctrine->getRepository(GroupeEtudiant::class)->randRow());

            $manager->persist($cours);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SalleFixtures::class,
            EnseignantFixtures::class,
            GroupeEtudiantFixtures::class
        ];
    }
}
