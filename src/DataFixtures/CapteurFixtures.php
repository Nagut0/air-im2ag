<?php

namespace App\DataFixtures;

use App\Entity\Capteur;
use App\Entity\Salle;
use App\Service\DoctrineRandom;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use function Symfony\Component\Clock\now;

class CapteurFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $capteur = new Capteur();
            $capteur->setDevEui("devEui$i");
            $capteur->setDescription("Ceci est la description du capteur n°$i");
            $capteur->setMarque($faker->word);
            $capteur->setReseau("Reseau$i");
            $capteur->setDateDebut(new \DateTime('2023-05-07 11:44:00'));
            $capteur->setDateFin(now());
            $capteur->setSalle($this->doctrine->getRepository(Salle::class)->randRow());

            $manager->persist($capteur);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SalleFixtures::class
        ];
    }
}
