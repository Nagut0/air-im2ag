<?php

namespace App\DataFixtures;

use App\Entity\TypeReleve;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TypeReleveFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $tab = ['co2', 'temperature', 'humidité'];

        foreach ($tab as $t){
            $type = new TypeReleve();
            $type->setLabel($t);

            $manager->persist($type);
        }

        $manager->flush();
    }
}
