<?php

namespace App\DataFixtures;

use App\Entity\Enseignant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EnseignantFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $enseignant = new Enseignant();
            $enseignant->setPrenom($faker->firstName);
            $enseignant->setNom($faker->lastName);
            $enseignant->setEmailEnseignant($faker->email);

            $manager->persist($enseignant);
        }

        $manager->flush();
    }
}
