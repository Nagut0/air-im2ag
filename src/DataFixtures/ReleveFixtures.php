<?php

namespace App\DataFixtures;

use App\Entity\Capteur;
use App\Entity\Releve;
use App\Entity\TypeReleve;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ReleveFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $releve = new Releve();
            $releve->setDate($faker->dateTime);
            $releve->setCapteur($this->doctrine->getRepository(Capteur::class)->randRow());
            $releve->setTypeReleve($this->doctrine->getRepository(TypeReleve::class)->randRow());
            if($releve->getTypeReleve()->getLabel() == 'co2'){
                $releve->setValeur($faker->randomNumber(3));
            } else if ($releve->getTypeReleve()->getLabel() == 'humidité'){
                $releve->setValeur($faker->numberBetween(0,100));
            } else if ($releve->getTypeReleve()->getLabel() == 'temperature'){
                $releve->setValeur($faker->randomFloat(1,0,60));
            }


            $manager->persist($releve);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CapteurFixtures::class,
            TypeReleveFixtures::class
        ];
    }
}
