<?php

namespace App\Entity;

use App\Repository\CoursRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CoursRepository::class)]
class Cours
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254, nullable: true)]
    private ?string $intitule_cours = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date_debut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date_fin = null;

    #[ORM\ManyToOne(inversedBy: 'cours')]
    private ?Salle $salle = null;

    #[ORM\ManyToOne(inversedBy: 'cours')]
    private ?Enseignant $enseignant = null;

    #[ORM\ManyToOne(inversedBy: 'cours')]
    private ?GroupeEtudiant $groupe_etu = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntituleCours(): ?string
    {
        return $this->intitule_cours;
    }

    public function setIntituleCours(?string $intitule_cours): self
    {
        $this->intitule_cours = $intitule_cours;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    public function getSalle(): ?Salle
    {
        return $this->salle;
    }

    public function setSalle(?Salle $salle): self
    {
        $this->salle = $salle;

        return $this;
    }

    public function getEnseignant(): ?Enseignant
    {
        return $this->enseignant;
    }

    public function setEnseignant(?Enseignant $enseignant): self
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    public function getGroupeEtu(): ?GroupeEtudiant
    {
        return $this->groupe_etu;
    }

    public function setGroupeEtu(?GroupeEtudiant $groupe_etu): self
    {
        $this->groupe_etu = $groupe_etu;

        return $this;
    }
}
