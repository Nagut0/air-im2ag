<?php

namespace App\Entity;

use App\Repository\GroupeEtudiantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupeEtudiantRepository::class)]
class GroupeEtudiant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254, nullable: true)]
    private ?string $code_vet = null;

    #[ORM\Column(length: 254)]
    private ?string $nom = null;

    #[ORM\Column]
    private ?int $nombre_etudiants = null;

    #[ORM\OneToMany(mappedBy: 'groupe_etu', targetEntity: Cours::class)]
    private Collection $cours;

    public function __construct()
    {
        $this->cours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeVet(): ?string
    {
        return $this->code_vet;
    }

    public function setCodeVet(?string $code_vet): self
    {
        $this->code_vet = $code_vet;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNombreEtudiants(): ?int
    {
        return $this->nombre_etudiants;
    }

    public function setNombreEtudiants(int $nombre_etudiants): self
    {
        $this->nombre_etudiants = $nombre_etudiants;

        return $this;
    }

    /**
     * @return Collection<int, Cours>
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCours(Cours $cours): self
    {
        if (!$this->cours->contains($cours)) {
            $this->cours->add($cours);
            $cours->setGroupeEtu($this);
        }

        return $this;
    }

    public function removeCours(Cours $cours): self
    {
        if ($this->cours->removeElement($cours)) {
            if ($cours->getGroupeEtu() === $this) {
                $cours->setGroupeEtu(null);
            }
        }

        return $this;
    }
}
