<?php

namespace App\Entity;

use App\Repository\ReleveRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReleveRepository::class)]
class Releve
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['shallow'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['shallow'])]
    private ?float $valeur = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['shallow'])]
    private ?\DateTimeInterface $date = null;

    #[ORM\ManyToOne(fetch: "EAGER", inversedBy: 'releves')]
//    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['shallow'])]
    private ?TypeReleve $type_releve = null;

    #[ORM\ManyToOne(inversedBy: 'releves')]
    private ?Salle $salle = null;

    #[ORM\ManyToOne(inversedBy: 'releves')]
    private ?Capteur $capteur = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?float
    {
        return $this->valeur;
    }

    public function setValeur(float $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTypeReleve(): ?TypeReleve
    {
        return $this->type_releve;
    }

    public function setTypeReleve(?TypeReleve $type_releve): self
    {
        $this->type_releve = $type_releve;

        return $this;
    }

    public function getSalle(): ?Salle
    {
        return $this->salle;
    }

    public function setSalle(?Salle $salle): self
    {
        $this->salle = $salle;

        return $this;
    }

    public function getCapteur(): ?Capteur
    {
        return $this->capteur;
    }

    public function setCapteur(?Capteur $capteur): self
    {
        $this->capteur = $capteur;

        return $this;
    }
}
