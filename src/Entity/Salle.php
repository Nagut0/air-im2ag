<?php

namespace App\Entity;

use App\Repository\SalleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SalleRepository::class)]
class Salle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254)]
    private ?string $nom_salle = null;

    #[ORM\Column(nullable: true)]
    private ?int $capacite = null;

    #[ORM\ManyToOne(inversedBy: 'salles')]
    private ?Batiment $batiment = null;

    #[ORM\OneToMany(mappedBy: 'salle', targetEntity: Cours::class)]
    private Collection $cours;

    #[ORM\OneToMany(mappedBy: 'salle', targetEntity: Releve::class)]
    private Collection $releves;

    public function __construct()
    {
        $this->cours = new ArrayCollection();
        $this->releves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomSalle(): ?string
    {
        return $this->nom_salle;
    }

    public function setNomSalle(string $nom_salle): self
    {
        $this->nom_salle = $nom_salle;

        return $this;
    }

    public function getCapacite(): ?int
    {
        return $this->capacite;
    }

    public function setCapacite(?int $capacite): self
    {
        $this->capacite = $capacite;

        return $this;
    }

    public function getBatiment(): ?Batiment
    {
        return $this->batiment;
    }

    public function setBatiment(?Batiment $batiment): self
    {
        $this->batiment = $batiment;

        return $this;
    }

    /**
     * @return Collection<int, Cours>
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCours(Cours $cours): self
    {
        if (!$this->cours->contains($cours)) {
            $this->cours->add($cours);
            $cours->setSalle($this);
        }

        return $this;
    }

    public function removeCours(Cours $cours): self
    {
        if ($this->cours->removeElement($cours)) {
            if ($cours->getSalle() === $this) {
                $cours->setSalle(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNomSalle();
    }

    /**
     * @return Collection<int, Releve>
     */
    public function getReleves(): Collection
    {
        return $this->releves;
    }

    public function addReleve(Releve $releve): self
    {
        if (!$this->releves->contains($releve)) {
            $this->releves->add($releve);
            $releve->setSalle($this);
        }

        return $this;
    }

    public function removeReleve(Releve $releve): self
    {
        if ($this->releves->removeElement($releve)) {
            if ($releve->getSalle() === $this) {
                $releve->setSalle(null);
            }
        }
        return $this;
    }
}
