<?php

namespace App\Entity;

use App\Repository\TypeReleveRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TypeReleveRepository::class)]
class TypeReleve
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['shallow'])]
    private ?int $id = null;

    #[ORM\Column(length: 254)]
    #[Groups(['shallow'])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'type_releve', targetEntity: Releve::class)]
    private Collection $releves;

    public function __construct()
    {
        $this->releves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, Releve>
     */
    public function getReleves(): Collection
    {
        return $this->releves;
    }

    public function addReleve(Releve $releve): self
    {
        if (!$this->releves->contains($releve)) {
            $this->releves->add($releve);
            $releve->setTypeReleve($this);
        }

        return $this;
    }

    public function removeReleve(Releve $releve): self
    {
        if ($this->releves->removeElement($releve)) {
            // set the owning side to null (unless already changed)
            if ($releve->getTypeReleve() === $this) {
                $releve->setTypeReleve(null);
            }
        }

        return $this;
    }
}
