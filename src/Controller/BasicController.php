<?php

namespace App\Controller;

use App\Entity\Capteur;
use App\Entity\Cours;
use App\Entity\Releve;
use App\Entity\Salle;
use App\Entity\TypeReleve;
use App\Service\DbFetcher;
use Cassandra\Date;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function PHPUnit\Framework\isNull;

#[Route('/')]
class BasicController extends AbstractController
{
    #[Route('/', name:'index')]
    public function index(ManagerRegistry $doctrine): Response{
        return $this->render('dashboard/index.html.twig');
    }



    /**
     * @throws NonUniqueResultException
     * @throws \Exception
     */
    #[Route('/fetchDB/{from}/{to}')]
    public function fetchDB(DbFetcher $fetcher, ManagerRegistry $doctrine, string $from, string $to): Response{
        $relRepo = $doctrine->getRepository(Releve::class);
        $earliest = $relRepo->getReleves(nb_results: 1);
        $latest = $relRepo->getReleves(latest: true, nb_results: 1);
        $start = new DateTime($from);
        $end = new DateTime($to);

        if ($start > $end){
            return $this->redirectToRoute('fetch.interface');
        }

        if (( $earliest == null) or ($latest == null)){ //BOF
            $fetcher->fetchDB($start, $end);
        } else {

            //On décale de 1 seconde pour ne pas récupérer à nouveau les enregistrements de latest et earliest
            $earliest = $earliest[0]->getDate()->sub(new \DateInterval('PT1S'));
            $latest = $latest[0]->getDate()->add(new \DateInterval('PT1S'));

            if ($start < $earliest){
                $fetcher->fetchDB($start, $earliest);
            }
            if ($end > $latest) {
                $fetcher->fetchDB($latest, $end);
            }
        }

        return $this->redirectToRoute('fetch.interface');
    }

    #[Route('/fetchInterface', name: 'fetch.interface')]
    public function fetchInterface(DbFetcher $fetcher, ManagerRegistry $doctrine): Response{
        $relRepo = $doctrine->getRepository(Releve::class);
        $earliest = $relRepo->getReleves(nb_results: 1);
        $latest = $relRepo->getReleves(latest: true, nb_results: 1);

        if ($earliest){
            $earliest = $earliest[0]->getDate();
        }
        if ($latest) {
            $latest = $latest[0]->getDate();
        }

        return $this->render('dashboard/database_state.html.twig', [
            'earliest' => $earliest,
            'latest' => $latest
        ]);
    }

    #[Route('/home/{from}/{to}/{co2_threshold?400}/{hum_threshold?50}/{temp_threshold?28}', name: 'home')]
    public function home(ManagerRegistry $doctrine, int $co2_threshold, int $hum_threshold, float $temp_threshold, string $from = null, string $to = null): Response{
        $relRepo = $doctrine->getRepository(Releve::class);

        // Récupération des différents typeReleve à utiliser pour spécifier les requêtes suivantes
        $typeC = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'co2'], [], 1, 0)[0];
        $typeH = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'humidite'], [], 1, 0)[0];
        $typeT = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'temperature'], [], 1, 0)[0];

        if (($from == null) or ($to == null)) {
            $to = $relRepo->getReleves(latest: true, nb_results: 1);
            if($to){
                $to = $to[0]->getDate();
            } else {
                $to = new \DateTime('2023-06-07 00:00:00');    //RANDOM
            }
            $from = clone $to;
            $from = $from->sub(new \DateInterval('P7D'));
        } else {
            $from = new \DateTime($from);
            $to = new \DateTime($to);
        }

        $tempC = $relRepo->getNbReleves($from, $to, threshold: $co2_threshold, type: $typeC );
        $tempH = $relRepo->getNbReleves($from, $to, threshold: $hum_threshold, type: $typeH );
        $tempT = $relRepo->getNbReleves($from, $to, threshold: $temp_threshold, type: $typeT );

        return $this->render('dashboard/home.html.twig', [
            'co2_data' => $tempC,
            'hum_data' => $tempH,
            'temp_data' => $tempT,
            'co2_threshold' => $co2_threshold,
            'hum_threshold' => $hum_threshold,
            'temp_threshold' => $temp_threshold,
            'from' => $from,
            'to' => $to
        ]);
    }

}