<?php

namespace App\Controller;

use App\Entity\Capteur;
use App\Entity\Releve;
use App\Entity\TypeReleve;
use App\Form\CapteurType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;

class CapteurController extends AbstractController
{
    #[Route('/capteurs', name: 'capteurs.list')]
    public function listeCapteurs(ManagerRegistry $doctrine): Response{
        $capteurs = $doctrine->getRepository(Capteur::class)->findAll();

        return $this->render('dashboard/listeCapteurs.html.twig', [
            'data' => $capteurs,
        ]);
    }

    #[Route('/capteurs/delete/{id}', name:'capteurs.delete')]
    public function deleteCapteur(ManagerRegistry $doctrine, Capteur $capteur = null): RedirectResponse{
        if ($capteur){
            $manager = $doctrine->getManager();
            $manager->remove($capteur);
            $manager->flush();
            $this->addFlash('success', "Le capteur a été supprimé avec succès");
        } else {
            $this->addFlash('error', "Capteur inexistant");
        }
        return $this->redirectToRoute('capteurs.list');
    }

    #[Route('api/capteurs/{id}/{from?2023-06-06 00:00:00}/{to?2023-06-07 00:00:00}', name:'api.capteurs.releves')]
    public function fetchReleves2(int $id, string $from, string $to, ManagerRegistry $doctrine, SerializerInterface $serializer): Response {

        $capteur = $doctrine->getRepository(Capteur::class)->find($id);

        $rel_repo = $doctrine->getRepository(Releve::class);

        // Récupération des différents typeReleve à utiliser pour spécifier les requêtes suivantes
        $typeC = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'co2'], [], 1, 0)[0];
        $typeH = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'humidite'], [], 1, 0)[0];
        $typeT = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'temperature'], [], 1, 0)[0];

        if (($from == null) or ($to == null)) {
            $to = $rel_repo->getReleves(latest: true, nb_results: 1);
            if($to){
                $to = $to[0]->getDate();
            } else {
                $to = new \DateTime('2023-06-07 00:00:00');    //RANDOM
            }
            $from = clone $to;
            $from = $from->sub(new \DateInterval('P7D'));
        } else {
            $from = new \DateTime($from);
            $to = new \DateTime($to);
        }

        $co2_releves = $rel_repo->getReleves($typeC, $capteur, start: $from, end: $to, forGraph: true);
        $hum_releves = $rel_repo->getReleves($typeH, $capteur, start: $from, end: $to, forGraph: true);
        $temp_releves = $rel_repo->getReleves($typeT, $capteur, start: $from, end: $to, forGraph: true);

        $context = (new ObjectNormalizerContextBuilder())->withGroups('shallow')->toArray();

        $data = [];
        foreach ($co2_releves as $r){
            $data[0]['l'][] = $r['date'];
            $data[0]['v'][] = $r['valeur'];
        }
        foreach ($hum_releves as $r){
            $data[1]['l'][] = $r['date'];
            $data[1]['v'][] = $r['valeur'];
        }
        foreach ($temp_releves as $r){
            $data[2]['l'][] = $r['date'];
            $data[2]['v'][] = $r['valeur'];
        }

        return new Response($serializer->serialize($data, 'json', $context));
    }

    #[Route('/capteurs/{id}/{from}/{to}', name:'capteurs.details')]
    public function details_capteur(int $id, ManagerRegistry $doctrine, SerializerInterface $serializer, string $from = null, string $to = null,): Response
    {
        $capteur = $doctrine->getRepository(Capteur::class)->find($id);

        $rel_repo = $doctrine->getRepository(Releve::class);

        // Récupération des différents typeReleve à utiliser pour spécifier les requêtes suivantes
        $typeC = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'co2'], [], 1, 0)[0];
        $typeH = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'humidite'], [], 1, 0)[0];
        $typeT = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'temperature'], [], 1, 0)[0];

        if (($from == null) or ($to == null)) {
            $to = $rel_repo->getReleves(latest: true, nb_results: 1);
            if($to){
                $to = $to[0]->getDate();
            } else {
                $to = new \DateTime('2023-06-07 00:00:00');    //RANDOM
            }
            $from = clone $to;
            $from = $from->sub(new \DateInterval('P7D'));
        } else {
            $from = new \DateTime($from);
            $to = new \DateTime($to);
        }

        $co2_releves = $rel_repo->getReleves($typeC, $capteur, start: $from, end: $to, forGraph: true);
        $hum_releves = $rel_repo->getReleves($typeH, $capteur, start: $from, end: $to, forGraph: true);
        $temp_releves = $rel_repo->getReleves($typeT, $capteur, start: $from, end: $to, forGraph: true);

        $context = (new ObjectNormalizerContextBuilder())->withGroups('shallow')->toArray();

        $salle = $rel_repo->getSalleFromCapteur($capteur);

        return $this->render('dashboard/details_capteur.html.twig', [
            'capteur' => $capteur,
            'salle' => $salle,
            'co2' => $serializer->serialize($co2_releves, 'json', $context),
            'humidite' => $serializer->serialize($hum_releves, 'json', $context),
            'temperature' => $serializer->serialize($temp_releves, 'json', $context),
            'from' => $from,
            'to' => $to
        ]);
    }
}
