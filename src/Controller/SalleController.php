<?php

namespace App\Controller;

use App\Entity\Releve;
use App\Entity\Salle;
use App\Entity\TypeReleve;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SalleController extends AbstractController
{
    #[Route('/salles/stats/{from?2023-06-06 00:00:00}/{to?2023-06-07 00:00:00}', name: 'api.salles.list')]
    public function getSalleStats(ManagerRegistry $doctrine, string $from, string $to): Response{

        $relRepo = $doctrine->getRepository(Releve::class);
        $salles = $doctrine->getRepository(Salle::class)->findAll();
        $data = [];
        foreach ($salles as $s){
            $stats = $relRepo->getStatsSalle($s, new \DateTime($from) , new \DateTime($to));
            $temp = array('','','','','','','','','','');
            $temp[0] = '<a href="/salles/'.$s->getId().'">'.$s->getNomSalle().'</a>';
            foreach ($stats as $st){
                if($st['label'] == 'co2'){
                    $temp[1] = $st[1];
                    $temp[2] = round($st[2],0);
                    $temp[3] = $st[3];
                } elseif ($st['label'] == 'humidité') {
                    $temp[4] = $st[1];
                    $temp[5] = round($st[2],1);
                    $temp[6] = $st[3];
                } elseif ($st['label'] == 'temperature') {
                    $temp[7] = $st[1];
                    $temp[8] = round($st[2],1);
                    $temp[9]= $st[3];
                }
            }
            $data[] = $temp;
        }

        return $this->json($data);
    }

    #[Route('/salles/{id}', name:'salle.details')]
    public function details_salle(int $id, ManagerRegistry $doctrine): Response{
        $relRepo = $doctrine->getRepository(Releve::class);
        $salle = $doctrine->getRepository(Salle::class)->find($id);

        // Récupération des différents typeReleve à utiliser pour spécifier les requêtes suivantes
        $typeC = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'co2'], [], 1, 0)[0];
        $typeH = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'humidite'], [], 1, 0)[0];
        $typeT = $doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'temperature'], [], 1, 0)[0];

        $co2 = $relRepo->getReleves($typeC, salle: $salle, latest: true, nb_results: 1);
        if($co2 != null){
            $co2 = $co2[0];
        } else {
            $co2 = null;
        }

        $temperature = $relRepo->getReleves($typeT, salle: $salle, latest: true, nb_results: 1);
        if($temperature != null){
            $temperature = $temperature[0];
        } else {
            $temperature = null;
        }

        $humidite = $relRepo->getReleves($typeH, salle: $salle, latest: true, nb_results: 1);
        if($humidite != null){
            $humidite = $humidite[0];
        } else {
            $humidite = null;
        }

        return $this->render('dashboard/details_salle.html.twig', [
            'salle' => $salle,
            'capteurs' => $relRepo->getCapteursFromSalle($salle),
            'co2'=> $co2,
            'temperature' => $temperature,
            'humidite' => $humidite,
        ]);
    }

    #[Route('/salles/{from}/{to}', name: 'salles.list')]
    public function listeSalles(ManagerRegistry $doctrine, string $from = null, string $to = null): Response{
        $relRepo = $doctrine->getRepository(Releve::class);
        $salles = $doctrine->getRepository(Salle::class)->findAll();
        $data = [];

        if (($from == null) or ($to == null)) {
            $to = $relRepo->getReleves(latest: true, nb_results: 1);
            if($to){
                $to = $to[0]->getDate();
            } else {
                $to = new \DateTime('2023-06-07 00:00:00');
            }
            $from = clone $to;
            $from = $from->sub(new \DateInterval('P7D'));
        } else {
            $from = new \DateTime($from);
            $to = new \DateTime($to);
        }

        foreach ($salles as $s){
            $stats = $relRepo->getStatsSalle($s,$from,$to);
            $temp = [];

            foreach ($stats as $st){
                if($st['label'] == 'co2'){
                    $temp['c'][0] = $st[1];
                    $temp['c'][1] = round($st[2],0);
                    $temp['c'][2] = $st[3];
                } elseif ($st['label'] == 'humidité') {
                    $temp['h'][0] = $st[1];
                    $temp['h'][1] = round($st[2],1);
                    $temp['h'][2] = $st[3];
                } elseif ($st['label'] == 'temperature') {
                    $temp['t'][0] = $st[1];
                    $temp['t'][1] = round($st[2],1);
                    $temp['t'][2] = $st[3];
                }
            }
            $data[$s->getNomSalle()] = $temp;
        }

        return $this->render('dashboard/listeSalles.html.twig', [
            'data' => $data,
            'salles' => $salles,
            'from' => $from,
            'to' => $to
        ]);
    }
}
