<?php

namespace App\Service;

use App\Entity\Batiment;
use App\Entity\Capteur;
use App\Entity\Releve;
use App\Entity\Salle;
use App\Entity\TypeReleve;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DbFetcher
{
    public function __construct(private ManagerRegistry $doctrine, private HttpClientInterface $client)
    {
    }

    # Fonction utilisé pour rapatrier des données distantes dans la BD dans la plage de date définie
    public function fetchDB(\DateTime $from, \DateTime $to){
        $manager = $this->doctrine->getManager();

        // Récupération des différents typeReleve à utiliser pour spécifier les requêtes suivantes
        $typeC = $this->doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'co2'], [], 1, 0)[0];
        $typeH = $this->doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'humidite'], [], 1, 0)[0];
        $typeT = $this->doctrine->getRepository(TypeReleve::class)->findBy(['label' => 'temperature'], [], 1, 0)[0];

        $startDate = "'".$from->format('Y-m-d')."'";
        $endDate = "'".$to->format('Y-m-d')."'";

        $r = $this->client->request(
            'GET',
            "http://iot-monitor.u-ga.fr:8086/query?db=lorawan&q=".rawurlencode("SELECT co2, temperature, humidity, deviceName FROM data WHERE time >= ".$startDate." and time <= ".$endDate." and deviceName !~ /022.*/ and deviceName !~ /ERS.*/ GROUP BY devEUI ORDER BY time DESC")
        );

        $request = json_decode($r->getContent(), true);
        if (!isset($request['results'][0]['series'])) {
            dump('Aucune donnée trouvée: '.$from->format('Y-m-d').' -> '.$to->format('Y-m-d'));
            return;
        }
        $series = $request['results'][0]['series'];
        foreach ($series as $s){
            $capteur = $this->doctrine->getRepository(Capteur::class)->findByDevEui($s['tags']['devEUI']);
            if (!$capteur){
                $capteur = new Capteur();
                $capteur->setDevEui($s['tags']['devEUI']);
                $capteur->setDescription('Nouveau capteur');
                $capteur->setMarque('???');
                $capteur->setReseau('???');

                $manager->persist($capteur);
            }

            $salle = $this->doctrine->getRepository(Salle::class)->findByNomSalle($s['values'][0][4]);
            if (!$salle){
                $salle = new Salle();
                $salle->setNomSalle($s['values'][0][4]);
                $salle->setBatiment($this->doctrine->getRepository(Batiment::class)->randRow());
                $salle->setCapacite(0);
                $manager->persist($salle);
            }

            foreach ($s['values'] as $rel){
                if ($rel[1] != null){
                    $releve = new Releve();
                    $releve->setDate(new \DateTime($rel[0]));
                    $releve->setCapteur($capteur);
                    $releve->setSalle($salle);
                    $releve->setTypeReleve($typeC);
                    $releve->setValeur($rel[1]);
                    $manager->persist($releve);
                }

                if ($rel[2] != null){
                    $releve = new Releve();
                    $releve->setDate(new \DateTime($rel[0]));
                    $releve->setCapteur($capteur);
                    $releve->setSalle($salle);
                    $releve->setTypeReleve($typeT);
                    $releve->setValeur($rel[2]);
                    $manager->persist($releve);
                }

                if ($rel[3] != null){
                    $releve = new Releve();
                    $releve->setDate(new \DateTime($rel[0]));
                    $releve->setCapteur($capteur);
                    $releve->setSalle($salle);
                    $releve->setTypeReleve($typeH);
                    $releve->setValeur($rel[3]);
                    $manager->persist($releve);
                }
            }

            $manager->flush();
        }
    }
}