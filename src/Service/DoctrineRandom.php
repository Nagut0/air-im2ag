<?php

namespace App\Service;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\Persistence\ManagerRegistry;

class DoctrineRandom
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    # Renvoie une ligne aléatoire de la table correpondant à l'entité passée en paramètre
    public function randRow(string $entityClass) {
        $all = $this->doctrine->getRepository($entityClass)->findAll();
        shuffle($all);
        return $all[0];
    }
}